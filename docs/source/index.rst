.. nwgraph documentation master file, created by
   sphinx-quickstart on Fri Jun 16 14:31:57 2023.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to nwgraph's documentation!
===================================


.. toctree::
   :maxdepth: 2
   :caption: Basics

   basics/welcome
   basics/pathways
   basics/message_passing

.. toctree::
   :maxdepth: 2
   :caption: Examples

   examples/gcn

.. toctree::
   :maxdepth: 2
   :caption: Docstring

   docstring/nwgraph

.. toctree::
   :maxdepth: 2
   :caption: Index

   indices
