# Message Passing

The library is built around the idea of message passing. The main class for this pattern is under {doc}`MessagePassing </docstring/nwgraph.message_passing>` module abstract class.

The general code for a message passing layer looks like this:

```python
MessagePassingType = tr.Tensor | dict[str, tr.Tensor] # tr.Tensor for GCN
NodesStateType = tr.Tensor | dict[str, tr.Tensor] # tr.Tensor for GCN
class MessagePassing(nn.Module, ABC):
    def __init__(self, graph: Graph):
        super().__init__()
        self.graph = graph

    @abstractmethod
    def message_pass(self) -> MessagePassingType:
        """Method that defines how messages are sent in one iteration."""

    @abstractmethod
    def aggregate(self, messages: MessagePassingType) -> NodesStateType:
        """Method that defines how message are aggregated"""

    @abstractmethod
    def update(self, aggregation: NodesStateType) -> NodesState:
        """Method that defines how node states are updated"""

    def forward(self, x: tr.Tensor) -> Graph:
        self.graph = self.graph.clone() # clone the received graph
        self.graph.nodes_state = NodesState(self.graph.node_names,  x) # set the states to the input tensor
        messages = self.message_pass() # 1) call message passing
        aggregation = self.aggregate(messages) # 2) call aggregate
        new_states: NodesState = self.update(aggregation) # 3) call update
        self.graph.nodes_state = new_states # set states to cloned graph
        return self.graph # return it
```

We can see that the algorithm for all message passing layers has 3 components:
- receiving the graph that is passed at constructor time
- setting the nodes state to whatever input we received to `.forward` method
- call `message_pass`, `aggregate` and `update`. These functions have access `self.nodes_state` and `self.graph` for their implementation.
- create a new graph with updated states and return it. Only nodes/edges are copied, not edge networks for example.

Next steps: see {doc}`/examples/gcn` for a more concrete example of a MP implementation.
