#!/usr/bin/sh

export FILE_ABS_PATH=$(readlink -f ${BASH_SOURCE[0]})
export CWD=$(dirname $FILE_ABS_PATH)

rm -rf ${CWD}/source/docstring
sphinx-apidoc ${CWD}/../nwgraph -o ${CWD}/source/docstring

cd ${CWD}
make clean
make html
cd --
