"""nwgraph logger singleton"""
from loggez import make_logger

nwg_logger = make_logger("NWGRAPH")
