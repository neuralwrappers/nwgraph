torch==2.5.1
tqdm==4.67.1
overrides==7.7.0
graphviz==0.20.1
colorama==0.4.6
lovely_tensors==0.1.17
numpy==1.26.4
loggez==0.4.2
