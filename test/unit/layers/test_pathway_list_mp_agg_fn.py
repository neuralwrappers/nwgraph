from torch import nn
import torch as tr

from nwgraph.graph.node import DimNode
from nwgraph.graph import BipartiteGraph
from nwgraph.layers import PathwayListMp
from nwgraph.pathway import PathwayList as PL, Pathway as P, ConvergingPathways as CP

from test_pathway_list_mp import build_big_graph, check_load_save_mp_equal

def test_PathwayListMp_agg_fn_1():
    """just a string for all nodes and all time steps"""
    MB = 5
    graph, pathways = build_big_graph()
    agg_fns = "mean"
    mp_layer = PathwayListMp(graph, nn.Linear, agg_fns, pathways)
    dims = {n: graph.node_args[graph.node_names.index(n)]["dims"][0] for n in graph.node_names}
    x = {k: tr.randn(MB, dims[k]) for k in graph.nodes_type_a}
    y = mp_layer(x)
    for k, v in y.readout().items():
        assert v.shape == (MB, dims[k])
    check_load_save_mp_equal(mp_layer, x, y)

def test_PathwayListMp_agg_fn_2():
    """explicit node name, trying all 3 string type combinations"""
    MB = 5
    graph, pathways = build_big_graph()
    agg_fns = {node.name: "mean" for node in graph.nodes}
    mp_layer = PathwayListMp(graph, nn.Linear, agg_fns, pathways)
    dims = {n: graph.node_args[graph.node_names.index(n)]["dims"][0] for n in graph.node_names}
    x = {k: tr.randn(MB, dims[k]) for k in graph.nodes_type_a}
    y = mp_layer(x)
    for k, v in y.readout().items():
        assert v.shape == (MB, dims[k])
    check_load_save_mp_equal(mp_layer, x, y)

    agg_fns = {node.name: "median" for node in graph.nodes}
    mp_layer = PathwayListMp(graph, nn.Linear, agg_fns, pathways)
    dims = {n: graph.node_args[graph.node_names.index(n)]["dims"][0] for n in graph.node_names}
    x = {k: tr.randn(MB, dims[k]) for k in graph.nodes_type_a}
    y = mp_layer(x)
    for k, v in y.readout().items():
        assert v.shape == (MB, dims[k])
    check_load_save_mp_equal(mp_layer, x, y)

    agg_fns = {node.name: "sum" for node in graph.nodes}
    mp_layer = PathwayListMp(graph, nn.Linear, agg_fns, pathways)
    dims = {n: graph.node_args[graph.node_names.index(n)]["dims"][0] for n in graph.node_names}
    x = {k: tr.randn(MB, dims[k]) for k in graph.nodes_type_a}
    y = mp_layer(x)
    for k, v in y.readout().items():
        assert v.shape == (MB, dims[k])
    check_load_save_mp_equal(mp_layer, x, y)

def test_PathwayListMp_agg_fn_3():
    """invalid node name"""
    graph, pathways = build_big_graph()
    agg_fns = {node.name + "a": "mean" for node in graph.nodes}
    try:
        _ = PathwayListMp(graph, nn.Linear, agg_fns, pathways)
    except KeyError:
        pass

def test_PathwayListMp_agg_fn_4():
    """a list of strings, one for each timestep, but common across nodes"""
    MB = 5
    graph, pathways = build_big_graph()
    agg_fns = ["mean"] * PL(pathways).n_steps
    mp_layer = PathwayListMp(graph, nn.Linear, agg_fns, pathways)
    dims = {n: graph.node_args[graph.node_names.index(n)]["dims"][0] for n in graph.node_names}
    x = {k: tr.randn(MB, dims[k]) for k in graph.nodes_type_a}
    y = mp_layer(x)
    for k, v in y.readout().items():
        assert v.shape == (MB, dims[k])
    check_load_save_mp_equal(mp_layer, x, y)

def test_PathwayListMp_agg_fn_5():
    """a list of strings, one for each timestep and one for each node"""
    MB = 5
    graph, pathways = build_big_graph()
    agg_fns = ["mean", "median", "sum"]
    mp_layer = PathwayListMp(graph, nn.Linear, agg_fns, pathways)
    dims = {n: graph.node_args[graph.node_names.index(n)]["dims"][0] for n in graph.node_names}
    x = {k: tr.randn(MB, dims[k]) for k in graph.nodes_type_a}
    y = mp_layer(x)
    for k, v in y.readout().items():
        assert v.shape == (MB, dims[k])
    check_load_save_mp_equal(mp_layer, x, y)

def test_PathwayListMp_agg_fn_6():
    """explicit agg fn callable"""
    class MyAggFn(nn.Module):
        def __init__(self, in_voters: int):
            super().__init__()
            self.in_voters = in_voters
            self.fc = nn.Linear(in_voters, out_features=1)

        def forward(self, votes: tr.Tensor, voters: list[P]):
            # reshape like this so we can go from (V, B, D) to (B, D) using a linear of (V, 1)
            return self.fc(votes.reshape(len(votes), -1).T).reshape(votes.shape[1:])

    MB = 5
    graph, pathways = build_big_graph()
    sim = pathways.simulate_mp()
    sim_lens = [{k: len(v) for k, v in sim_t.items()} for sim_t in sim]

    # skip t=0 because it's populated by input data already
    agg_fns = []
    for t in range(1, len(sim_lens)):
        t_aggs = {}
        for node, node_voters in sim_lens[t].items():
            if node_voters == 0:
                continue
            t_aggs[node] = MyAggFn(in_voters=node_voters)
        agg_fns.append(t_aggs)

    mp_layer = PathwayListMp(graph, nn.Linear, agg_fns, pathways)
    dims = {n: graph.node_args[graph.node_names.index(n)]["dims"][0] for n in graph.node_names}
    x = {k: tr.randn(MB, dims[k]) for k in graph.nodes_type_a}
    y = mp_layer(x)
    for k, v in y.readout().items():
        assert v.shape == (MB, dims[k])

    # save and load the weights in another identical layer
    check_load_save_mp_equal(mp_layer, x, y)


def test_PathwayListMp_ngclib_err_1():
    node_names = ["0", "1", "4", "5"]
    edge_indexes = [("0", "4"), ("1", "4"), ("0", "5"), ("1", "5"), ("4", "5")]
    g = BipartiteGraph(edge_indexes, nodes_type_a=["0", "1"], node_types=DimNode,
                       node_args=[{"dims": (3, )} for _ in node_names], node_names=node_names)
    pathways = PL([P(["0", "4"]), P(["1", "4"]), P(["0", "5"]), P(["1", "5"]),
                  P([CP([P(["0", "4"]), P(["1", "4"])]), "5"])])
    mp_layer = PathwayListMp(g, nn.Linear, "mean", pathways)

    mb = 5
    x = {node.name: tr.randn(mb, node.dims[0]) for node in g.nodes_type_a}
    y = mp_layer(x)
    y_read = y.readout()
    assert len(y_read) == 4
    for node_name, y_out in y_read.items():
        if node_name in [x.name for x in g.nodes_type_a]:
            assert tr.allclose(x[node_name], y_out)

if __name__ == "__main__":
    test_PathwayListMp_ngclib_err_1()
