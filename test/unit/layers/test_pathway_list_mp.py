from tempfile import TemporaryDirectory
from pathlib import Path
from torch import nn
import torch as tr

from nwgraph.graph.node import DimNode
from nwgraph.graph import Graph, BipartiteGraph
from nwgraph.layers import PathwayListMp
from nwgraph.pathway import PathwayList as PL, Pathway as P, ConvergingPathways as CP

def count_mp_ids(mp_layer: PathwayListMp) -> tuple[list[int], set[int]]:
    unique_ids = set()
    total_ids = []
    for pathway, pathway_models in mp_layer.pathway_list_models.items():
        for model in pathway_models.values():
            unique_ids.add(id(model))
            total_ids.append(id(model))
    return total_ids, unique_ids

def build_big_graph():
    edge_indexes = [("a", "e"), ("b", "e"), ("c", "f"), ("d", "f"), ("e", "g"), ("f", "g"), ("g", "h")]
    dims = {"a": (2, ), "b": (4, ), "c": (6, ), "d": (8, ), "e": (10, ), "f": (12, ), "g": (14, ), "h": (16, )}
    nodes_type_a = ["a", "b", "c", "d"]

    graph = BipartiteGraph(edge_indexes, nodes_type_a=nodes_type_a, node_types=DimNode,
                           node_args=[{"dims": dims[node_name]} for node_name in dims],
                           node_names=["a", "b", "c", "d", "e", "f", "g", "h"])
    pathways = PL([
        P([
            CP([
                P([CP([P(["a", "e"]), P(["b", "e"])]), "g"]),
                P([CP([P(["c", "f"]), P(["d", "f"])]), "g"])
            ]), "h"
        ]),
        P([CP([P(["a", "e"]), P(["b", "e"])]), "g", "h"]),
    ])
    return graph, pathways

def check_load_save_mp_equal(mp_layer: PathwayListMp, x, y):
    tmp_dir = Path(TemporaryDirectory().name)
    mp_layer.save_all_weights(tmp_dir)
    mp_layer2 = PathwayListMp(mp_layer.graph, mp_layer.model_types, mp_layer.ensemble_type, mp_layer.pathways)
    mp_layer2.load_all_weights(tmp_dir)
    y2 = mp_layer2(x).readout()
    for k, v in y.readout().items():
        assert tr.allclose(v, y2[k])

def test_PathwayListMp_bad():
    """invalid node in the pathway"""
    edge_indexes = [("a", "b"), ("b", "c"), ("b", "d"), ("c", "d"), ("c", "e"), ("d", "c")]
    dims = {"a": (2, ), "b": (4, ), "c": (6, ), "d": (8, ), "e": (9, )}
    graph = BipartiteGraph(edge_indexes, nodes_type_a=["a", "b"], node_types=DimNode,
                           node_args=[{"dims": dims[node_name]} for node_name in dims],
                           node_names=["a", "b", "c", "d", "e"])
    pathways = PL([
        P(["a", "b", "c", "d"]),
        P(["a", "b", "d", "c", "f"]), # f doesn"t exist
        P(["a", "b", "c", "e"]),
    ])
    try:
        _ = PathwayListMp(graph, nn.Linear, "mean", pathways)
        raise Exception
    except AssertionError as e:
        assert "New nodes or edges" in str(e), e

def test_PathwayListMp_line_models():
    """
    Build the pathway list for line pathways only.
    Even though there are many models, most of them are actually common (!) across pathways.

    Paths:
    - p1: a -> b -> c -> d
    - p2: a -> b -> d -> c -> e
    - p3: a -> b -> c -> e

    Models:
    - a -> b and b -> c are sharable
    - c -> d (p1), d -> c (p2), c -> e (p2) and (!!) c -> e (p3) are independent.

    """
    edge_indexes = [("a", "b"), ("b", "c"), ("b", "d"), ("c", "d"), ("c", "e"), ("d", "c")]
    dims = {"a": (2, ), "b": (4, ), "c": (6, ), "d": (8, ), "e": (9, )}
    graph = BipartiteGraph(edge_indexes, nodes_type_a=["a"], node_types=DimNode,
                           node_args=[{"dims": dims[node_name]} for node_name in dims],
                           node_names=["a", "b", "c", "d", "e"])
    pathways = PL([
        P(["a", "b", "c", "d"]),
        P(["a", "b", "d", "c", "e"]),
        P(["a", "b", "c", "e"]),
    ])

    mp_layer = PathwayListMp(graph, nn.Linear, "mean", pathways)
    assert mp_layer.n_models == 7
    total_ids, unique_ids = count_mp_ids(mp_layer)
    assert len(total_ids) == 10  # 3 on the first pathway, 4 on the second, 3 on the third
    assert len(unique_ids) == 7  # ('a', 'b') is shared across 3 paths (so -2) and ('b', 'c') across 2 paths (so -1)

def test_PathwayListMp_pathway_merging_model_1():
    """
    The merging pattern:
    a --\
         --> c -> d
    b --/
    """
    edge_indexes = [("a", "c"), ("b", "c"), ("c", "d")]
    dims = {"a": (2, ), "b": (4, ), "c": (6, ), "d": (8, )}
    graph = BipartiteGraph(edge_indexes, nodes_type_a=["a", "b"], node_types=DimNode,
                           node_args=[{"dims": dims[node_name]} for node_name in dims],
                           node_names=["a", "b", "c", "d"])
    pathways = PL([
        P([CP([P(["a", "c"]), P(["b", "c"])]), "d"])
    ])

    mp_layer = PathwayListMp(graph, nn.Linear, "mean", pathways)
    total_ids, unique_ids = count_mp_ids(mp_layer)
    assert len(total_ids) == 3
    assert len(unique_ids) == 3


def test_PathwayListMp_pathway_merging_model_2():
    """
    The merging pattern, but more complicated (a->c is shared across two paths):
    a --\
         --> c -> d
    b --/
    a --> c -> d
    """
    edge_indexes = [("a", "c"), ("b", "c"), ("c", "d")]
    dims = {"a": (2, ), "b": (4, ), "c": (6, ), "d": (8, )}
    graph = BipartiteGraph(edge_indexes, nodes_type_a=["a", "b"], node_types=DimNode,
                           node_args=[{"dims": dims[node_name]} for node_name in dims],
                           node_names=["a", "b", "c", "d"])
    pathways = PL([
        P([CP([P(["a", "c"]), P(["b", "c"])]), "d"]),
        P(["a", "c", "d"]),
    ])

    mp_layer = PathwayListMp(graph, nn.Linear, "mean", pathways)
    total_ids, unique_ids = count_mp_ids(mp_layer)
    assert len(total_ids) == 5
    assert len(unique_ids) == 4

def test_PathwayListMp_pathway_merging_model_3():
    """
    The merging pattern, but more complicated (a->d is shared across two paths):
    a --\
         --> d
    b --/
    c --/
    a -> d
    """
    edge_indexes = [("a", "d"), ("b", "d"), ("c", "d")]
    dims = {"a": (2, ), "b": (4, ), "c": (6, ), "d": (8, )}
    graph = BipartiteGraph(edge_indexes, nodes_type_a=["a", "b", "c"], node_types=DimNode,
                           node_args=[{"dims": dims[node_name]} for node_name in dims],
                           node_names=["a", "b", "c", "d"])
    pathways = PL([
        P([CP([P(["a", "d"]), P(["b", "d"]), P(["c", "d"])])]),
        P(["a", "d"]),
    ])

    mp_layer = PathwayListMp(graph, nn.Linear, "mean", pathways)
    total_ids, unique_ids = count_mp_ids(mp_layer)
    assert len(total_ids) == 4
    assert len(unique_ids) == 3

def test_PathwayListMp_pathway_merging_model_5():
    """
    The merging pattern, but ASCII art this time
    a --|
         --> e -
    b --|        |
                   g --> h
    c --|        |
         --> f -
    d --|

    a --|
         --> e --> g --> h
    b --|

    c --|
         --> f --> g --> h
    d --|

    """
    edge_indexes = [("a", "e"), ("b", "e"), ("c", "f"), ("d", "f"), ("e", "g"), ("f", "g"), ("g", "h")]
    dims = {"a": (2, ), "b": (4, ), "c": (6, ), "d": (8, ), "e": (10, ), "f": (12, ), "g": (14, ), "h": (16, )}
    graph = BipartiteGraph(edge_indexes, nodes_type_a=["a", "b"], node_types=DimNode,
                           node_args=[{"dims": dims[node_name]} for node_name in dims],
                           node_names=["a", "b", "c", "d", "e", "f", "g", "h"])
    pathways = PL([
        P([
            CP([
                P([CP([P(["a", "e"]), P(["b", "e"])]), "g"]),
                P([CP([P(["c", "f"]), P(["d", "f"])]), "g"])
            ]), "h"
        ]),
        P([CP([P(["a", "e"]), P(["b", "e"])]), "g", "h"]),
        P([CP([P(["c", "f"]), P(["d", "f"])]), "g", "h"]),
    ])

    mp_layer = PathwayListMp(graph, nn.Linear, "mean", pathways)
    total_ids, unique_ids = count_mp_ids(mp_layer)
    assert len(total_ids) == 15
    assert len(unique_ids) == 9
    assert len(mp_layer.all_models) == 9

def test_PathwayListMp_save_load_all_weights():
    edge_indexes = [("a", "e"), ("b", "e"), ("c", "f"), ("d", "f"), ("e", "g"), ("f", "g"), ("g", "h")]
    dims = {"a": (2, ), "b": (4, ), "c": (6, ), "d": (8, ), "e": (10, ), "f": (12, ), "g": (14, ), "h": (16, )}
    graph = BipartiteGraph(edge_indexes, nodes_type_a=["a", "b"], node_types=DimNode,
                           node_args=[{"dims": dims[node_name]} for node_name in dims],
                           node_names=["a", "b", "c", "d", "e", "f", "g", "h"])
    pathways = PL([
        P([
            CP([
                P([CP([P(["a", "e"]), P(["b", "e"])]), "g"]),
                P([CP([P(["c", "f"]), P(["d", "f"])]), "g"])
            ]), "h"
        ]),
        P([CP([P(["a", "e"]), P(["b", "e"])]), "g", "h"]),
        P([CP([P(["c", "f"]), P(["d", "f"])]), "g", "h"]),
    ])
    mp_layer = PathwayListMp(graph, nn.Linear, "mean", pathways)
    total_ids, unique_ids = count_mp_ids(mp_layer)

    tmp_dir = Path(TemporaryDirectory().name)
    mp_layer.save_all_weights(tmp_dir)
    mp_layer.load_all_weights(tmp_dir)


def test_PathwayListMp_forward_bad_input_data():
    MB = 5
    graph, pathways = build_big_graph()
    mp_layer = PathwayListMp(graph, nn.Linear, "mean", pathways)
    dims = {n: graph.node_args[graph.node_names.index(n)]["dims"][0] for n in graph.node_names}
    x = {k: tr.randn(MB, dims[k]) for k in ["a", "b"]}
    try:
        _ = mp_layer(x)
        raise ValueError
    except KeyError:
        pass

def test_PathwayListMp_forward():
    MB = 5
    graph, pathways = build_big_graph()
    mp_layer = PathwayListMp(graph, nn.Linear, "mean", pathways)
    dims = {n: graph.node_args[graph.node_names.index(n)]["dims"][0] for n in graph.node_names}
    x = {k: tr.randn(MB, dims[k]) for k in graph.nodes_type_a}
    assert mp_layer.n_steps == 3
    y = mp_layer(x)
    for k, v in y.readout().items():
        assert v.shape == (MB, dims[k])
    check_load_save_mp_equal(mp_layer, x, y)

def test_PathwayListMp_ngclib_err_1():
    node_names = ["0", "1", "4", "5"]
    edge_indexes = [("0", "4"), ("1", "4"), ("0", "5"), ("1", "5"), ("4", "5")]
    g = BipartiteGraph(edge_indexes, nodes_type_a=["0", "1"], node_types=DimNode,
                       node_args=[{"dims": (3, )} for node_name in node_names],
                       node_names=node_names)
    pathways = PL([P(["0", "4"]), P(["1", "4"]), P(["0", "5"]), P(["1", "5"]),
                  P([CP([P(["0", "4"]), P(["1", "4"])]), "5"])])
    mp_layer = PathwayListMp(g, nn.Linear, "mean", pathways)

    mb = 5
    x = {node.name: tr.randn(mb, node.dims[0]) for node in g.nodes_type_a}
    y = mp_layer(x)
    y_read = y.readout()
    assert len(y_read) == 4
    for node_name, y_out in y_read.items():
        if node_name in [x.name for x in g.nodes_type_a]:
            assert tr.allclose(x[node_name], y_out)

def test_PathwayListMp_temp_nodes_and_links_1():
    node_names = ["0", "1", "4"]
    edge_indexes = [["0", "1"], ["1", "4"]]
    g = Graph(edge_indexes, node_types=DimNode,
              node_args=[{"dims": (3, )} for node_name in node_names],
              node_names=node_names)
    pathways = PL([
        P([CP([P(["0", "0_1"]), P(["1", "0_1"])]), "4"]),
    ])
    mp_layer = PathwayListMp(g, nn.Linear, "mean", pathways, allow_new_nodes_or_edges=True)

    assert g.node_names == ["0", "1", "4"]
    assert mp_layer.graph.node_names == ["0", "1", "4", "0_1"]

    assert g.edge_indexes == [("0", "1"), ("1", "4")]
    assert set(mp_layer.graph.edge_indexes) == set([("0", "1"), ("1", "4"), ("0", "0_1"), ("0_1", "4"), ("1", "0_1")])


def test_PathwayListMp_temp_nodes_and_links_2():
    node_names = ["0", "1", "4"]
    edge_indexes = [["0", "1"], ["1", "4"]]
    g = BipartiteGraph(edge_indexes, node_types=DimNode,
                       node_args=[{"dims": (3, )} for node_name in node_names],
                       node_names=node_names, nodes_type_a=["0", "1"])
    pathways = PL([
        P([CP([P(["0", "0_1"]), P(["1", "0_1"])]), "4"]),
    ])
    mp_layer = PathwayListMp(g, nn.Linear, "mean", pathways, allow_new_nodes_or_edges=True)

    assert g.node_names == ["0", "1", "4"]
    assert mp_layer.graph.node_names == ["0", "1", "4", "0_1"]

    assert g.edge_indexes == [("0", "1"), ("1", "4")]
    assert set(mp_layer.graph.edge_indexes) == set([("0", "1"), ("1", "4"), ("0", "0_1"), ("0_1", "4"), ("1", "0_1")])

    assert g.nodes_type_b == ["4"]
    assert mp_layer.graph.nodes_type_b == ["4", "0_1"]

def test_PathwayListMp_temp_nodes_and_links_3():
    node_names = ["0", "1", "4"]
    edge_indexes = []
    g = BipartiteGraph(edge_indexes, nodes_type_a=["0", "1"], node_types=DimNode,
                       node_args=[{"dims": (3, )} for node_name in node_names],
                       node_names=node_names)
    pathways = PL([
        P([CP([P(["0", "0_1"]), P(["1", "0_1"])]), "4"]),
    ])

    mp_layer = PathwayListMp(g, nn.Linear, "mean", pathways, allow_new_nodes_or_edges=True)
    mb = 5
    x = {node.name: tr.randn(mb, node.dims[0]) for node in g.nodes_type_a}
    y = mp_layer(x)
    assert y.n_nodes_set == 4
    assert len(y.readout()) == 4
