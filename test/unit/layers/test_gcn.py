from nwgraph import Graph
from nwgraph.layers import GCN
import torch as tr

def test_gcn_output():
    edge_indexes = [("A", "E"), ("B", "E"), ("A", "F"), ("B", "F"), ("E", "F")]
    node_names = ["A", "B", "E", "F"]
    in_features, out_features = 50, 20
    g = Graph(edge_indexes, node_names=node_names)
    x = tr.randn(len(g.nodes), in_features)
    gcn = GCN(g, in_features, out_features)
    g2: Graph = gcn(x)
    y2 = g2.readout()

    assert len(g.nodes) == len(g2.nodes) == 4
    assert g.nodes == g2.nodes
    assert g.edges == g2.edges
    assert g.n_nodes_set == 0
    assert g2.n_nodes_set == 4
    assert y2.shape == (4, 20)

def test_gcn_memory_1():
    edge_indexes = [("A", "E"), ("B", "E"), ("A", "F"), ("B", "F"), ("E", "F")]
    node_names = ["A", "B", "E", "F"]
    in_features, out_features = 50, 20
    g = Graph(edge_indexes, node_names=node_names)
    x = tr.randn(len(g.nodes), in_features)

    gcn_no_mem = GCN(g, in_features, out_features)
    assert gcn_no_mem.is_memory_populated() is False
    _ = gcn_no_mem(x)
    assert gcn_no_mem.is_memory_populated() is False

    gcn = GCN(g, in_features, out_features, store_messages=True)
    assert gcn.is_memory_populated() is False
    g2: Graph = gcn(x)
    assert gcn.is_memory_populated() is True

    assert gcn.memory.messages.shape == (4, 20)
    assert gcn.memory.aggregation.shape == (4, 20)
    assert gcn.memory.update.state.shape == (4, 20)
    assert tr.allclose(g2.readout(), gcn.memory.update.state)

if __name__ == "__main__":
    test_gcn_output()
