from nwgraph import Graph
from nwgraph.graph.node import DimNode
from nwgraph.layers import EnsembleMp
from nwgraph.utils import compare_graph_states
import torch as tr
from torch import nn

def test_ensemble_mp_fail_no_DimNode():
    in_nodes = ["A", "B"]
    node_dims = (5, 7, 3, 3)
    x = {node: tr.randn(node_dim) for node, node_dim in zip(in_nodes, node_dims[0:2])}

    # This properly fails because Node has no in_dims which is needed for EnsembleMp
    try:
        edge_indexes = [("A", "E"), ("B", "E"), ("A", "F"), ("B", "F")]
        g = Graph(edge_indexes, node_names=["A", "B", "E", "F"])
        ens1 = EnsembleMp(g, edge_model_type=nn.Linear, ensemble_type="mean")
        _ = ens1(x)
        raise Exception
    except TypeError:
        pass

def test_ensemble_mp_fail_edge_wo_input_data():
    # This properly fails because E->F edge has no in data
    node_names = ["A", "B", "E", "F"]
    in_nodes = ["A", "B"]
    node_dims = (5, 7, 3, 3)
    x = {node: tr.randn(node_dim) for node, node_dim in zip(in_nodes, node_dims[0:2])}
    edge_indexes = [("A", "E"), ("B", "E"), ("A", "F"), ("B", "F"), ("E", "F")]
    node_args = [{"dims": (node_dim,)} for node_dim in node_dims]
    try:
        g = Graph(edge_indexes, node_names=node_names, node_types=DimNode, node_args=node_args)
        ens1 = EnsembleMp(g, edge_model_type=nn.Linear, ensemble_type="mean", all_edges_must_have_data=True)
        _ = ens1(x)
        raise Exception
    except KeyError:
        pass

def test_ensemble_mp_good():
    node_names = ["A", "B", "E", "F"]
    in_nodes = ["A", "B"]
    node_dims = (5, 7, 3, 3)
    x = {node: tr.randn(node_dim) for node, node_dim in zip(in_nodes, node_dims[0:2])}

    edge_indexes = [("A", "E"), ("B", "E"), ("A", "F"), ("B", "F")]
    node_args = [{"dims": (node_dim,)} for node_dim in node_dims]
    g = Graph(edge_indexes, node_names=node_names, node_types=DimNode, node_args=node_args)
    ens1 = EnsembleMp(g, nn.Linear, ensemble_type="mean")
    ens2 = EnsembleMp(g, nn.Linear, ensemble_type="mean")
    y: Graph = ens1(x)
    y2: Graph = ens2(y.readout())

    assert g.n_nodes_set == 0
    assert y.n_nodes_set == 4
    assert y2.n_nodes_set == 4
    for key in y.readout().keys():
        assert isinstance(key, str), type(key)
    assert tuple(compare_graph_states(y.readout(), y2.readout()).values()) == (True, True, False, False)

def test_ensemble_mp_agg_types():
    node_names = ["A", "B", "E", "F"]
    in_nodes = ["A", "B"]
    node_dims = (5, 7, 3, 3)
    x = {node: tr.randn(node_dim) for node, node_dim in zip(in_nodes, node_dims[0:2])}

    edge_indexes = [("A", "E"), ("B", "E"), ("A", "F"), ("B", "F")]
    node_args = [{"dims": (node_dim,)} for node_dim in node_dims]
    g = Graph(edge_indexes, node_names=node_names, node_types=DimNode, node_args=node_args)
    y_mean: Graph = EnsembleMp(g, nn.Linear, ensemble_type="mean")(x)
    y_mean2: Graph = EnsembleMp(g, nn.Linear, ensemble_type="mean")(x)
    y_median: Graph = EnsembleMp(g, nn.Linear, ensemble_type="median")(x)
    y_sum: Graph = EnsembleMp(g, nn.Linear, ensemble_type="sum")(x)
    assert tuple(compare_graph_states(y_mean.readout(), y_median.readout()).values()) == (True, True, False, False)
    assert tuple(compare_graph_states(y_mean.readout(), y_sum.readout()).values()) == (True, True, False, False)
    assert tuple(compare_graph_states(y_mean.readout(), y_mean2.readout()).values()) == (True, True, False, False)

def test_ensemble_mp_agg_types_shared_weights():
    node_names = ["A", "B", "E", "F"]
    in_nodes = ["A", "B"]
    node_dims = (5, 5, 3, 3)
    x = {node: tr.randn(node_dim) for node, node_dim in zip(in_nodes, node_dims[0:2])}

    edge_indexes = [("A", "E"), ("B", "E"), ("A", "F"), ("B", "F")]
    node_args = [{"dims": (node_dim,)} for node_dim in node_dims]
    g = Graph(edge_indexes, node_names=node_names, node_types=DimNode, node_args=node_args)
    common_linear = nn.Linear(5, 3)
    edges_model = {edge: common_linear for edge in g.edges}
    y_mean: Graph = EnsembleMp(g, edges_model, ensemble_type="mean")(x)
    y_mean2: Graph = EnsembleMp(g, edges_model, ensemble_type="mean")(x)
    y_median: Graph = EnsembleMp(g, edges_model, ensemble_type="median")(x)
    y_sum: Graph = EnsembleMp(g, edges_model, ensemble_type="sum")(x)
    assert tuple(compare_graph_states(y_mean.readout(), y_median.readout()).values()) == (True, True, False, False)
    assert tuple(compare_graph_states(y_mean.readout(), y_sum.readout()).values()) == (True, True, False, False)
    assert tuple(compare_graph_states(y_mean.readout(), y_mean2.readout()).values()) == (True, True, True, True)

def test_ensemble_mp_state_dict():
    node_names = ["A", "B", "E", "F"]
    node_dims = (5, 7, 3, 3)

    edge_indexes = [("A", "E"), ("B", "E"), ("A", "F"), ("B", "F")]
    node_args = [{"dims": (node_dim,)} for node_dim in node_dims]
    g = Graph(edge_indexes, node_names=node_names, node_types=DimNode, node_args=node_args)
    ens1 = EnsembleMp(g, nn.Linear, ensemble_type="mean")
    sd = ens1.state_dict()
    n_params = 0
    for edge in g.edges:
        edge_params = dict(ens1._pathway_models[edge].named_parameters())
        n_params += len(edge_params)
        # ens state dict stores parameters as {edge}.{param_name} for each edge model
        for key, value in edge_params.items():
            assert tr.allclose(sd[f"{edge}.{key}"], value)
    assert len(sd) == n_params

def test_ensemble_mp_memory_1():
    node_names = ["A", "B", "E", "F"]
    in_nodes = ["A", "B"]
    node_dims = (5, 5, 3, 3)
    x = {node: tr.randn(node_dim) for node, node_dim in zip(in_nodes, node_dims[0:2])}

    # we have an edge between two output nodes here. This will only be propagated at the second mp layer
    edge_indexes = [("A", "E"), ("B", "E"), ("A", "F"), ("B", "F"), ("E", "F")]
    node_args = [{"dims": (node_dim,)} for node_dim in node_dims]
    g = Graph(edge_indexes, node_names=node_names, node_types=DimNode, node_args=node_args)

    ens1_no_mem = EnsembleMp(g, nn.Linear, ensemble_type="mean")
    assert ens1_no_mem.is_memory_populated() == False
    _ = ens1_no_mem(x)
    assert ens1_no_mem.is_memory_populated() == False

    ens = EnsembleMp(g, nn.Linear, ensemble_type="mean", store_messages=True)
    assert ens.is_memory_populated() == False
    y: Graph = ens(x)
    assert ens.is_memory_populated() == True

    assert sorted(ens.memory.messages.keys()) == ["E", "F"]
    assert len(ens.memory.messages["E"]) == 2 and len(ens.memory.messages["F"]) == 2
    assert tr.allclose(ens.memory.aggregation["A"], x["A"])
    assert tr.allclose(ens.memory.aggregation["B"], x["B"])
    assert tr.allclose(ens.memory.aggregation["E"], sum(ens.memory.messages["E"]) / len(ens.memory.messages["E"]))
    assert tr.allclose(ens.memory.aggregation["F"], sum(ens.memory.messages["F"]) / len(ens.memory.messages["F"]))
    assert tr.allclose(ens.memory.update.state["A"], x["A"])
    assert tr.allclose(ens.memory.update.state["B"], x["B"])
    assert tr.allclose(ens.memory.update.state["E"], ens.memory.aggregation["E"])
    assert tr.allclose(ens.memory.update.state["F"], ens.memory.aggregation["F"])
    b4 = [x.clone() for x in ens.memory.messages["F"]]

    # pass through the same layer
    y2 = ens(y.readout())
    assert sorted(ens.memory.messages.keys()) == ["E", "F"]
    assert len(ens.memory.messages["E"]) == 2 and len(ens.memory.messages["F"]) == 3
    assert tr.allclose(ens.memory.aggregation["A"], x["A"])
    assert tr.allclose(ens.memory.aggregation["B"], x["B"])
    assert tr.allclose(ens.memory.aggregation["E"], sum(ens.memory.messages["E"]) / len(ens.memory.messages["E"]))
    # [A->F, B->F, E->F] / 3
    assert tr.allclose(ens.memory.aggregation["F"], sum([*b4, ens.memory.messages["F"][-1]]) / 3)
    assert tr.allclose(ens.memory.update.state["A"], x["A"])
    assert tr.allclose(ens.memory.update.state["B"], x["B"])
    assert tr.allclose(ens.memory.update.state["E"], ens.memory.aggregation["E"])
    assert tr.allclose(ens.memory.update.state["F"], ens.memory.aggregation["F"])

if __name__ == "__main__":
    test_ensemble_mp_agg_types()
