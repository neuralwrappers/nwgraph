from nwgraph.pathway import PathwayList as PL, Pathway as P, ConvergingPathways as CP

from list_equal_omg import list_equal_omg

def test_PathwayList_partial_pathways_1():
    path_list = PL([
        P(["a", "b", "c", "d"]),
        P(["a", "b", "d", "c", "e"]),
        P(["a", "b", "c", "e"]),
    ])
    assert path_list.all_nodes == {"a", "b", "c", "d", "e"}
    assert path_list.all_edges == [("a", "b"), ("b", "c"), ("b", "d"), ("c", "d"), ("c", "e"), ("d", "c")]
    assert list_equal_omg(path_list.partial_pathways, [
        ("a",),
        ("a", "b"),
        ("a", "b", "c"), ("a", "b", "d"), ("a", "b", "c", "d"),
        ("a", "b", "c", "e"), ("a", "b", "d", "c"),
        ("a", "b", "d", "c", "e")
    ])

def test_PathwayList_partial_pathways_2():
    path_list = PL([
        P([CP([P(["a", "c"]), P(["b", "c"])]), "d"]),
        P(["a", "c", "d"])
    ])
    assert len(path_list.partial_pathways) == 7
    assert list_equal_omg(path_list.partial_pathways, [
        ["a"],
        ["b"],
        ["a", "c"],
        ["a", "c", "d"],
        ["b", "c"],
        [CP([P(["a", "c"]), P(["b", "c"])])],
        P([CP([P(["a", "c"]), P(["b", "c"])]), "d"])
    ])
    assert path_list.all_nodes == {"a", "b", "c", "d"}
    assert path_list.all_edges == [("a", "c"), ("b", "c"), ("c", "d")]

def test_PathwayList_topological_sort():
    path_list = PL([
        P([
            CP([
                P([CP([P(["a", "e"]), P(["b", "e"])]), "g"]),
                P([CP([P(["c", "f"]), P(["d", "f"])]), "g"])
            ]), "h"
        ]),
        P([CP([P(["a", "e"]), P(["b", "e"])]), "g", "h"]),
        P([CP([P(["c", "f"]), P(["d", "f"])]), "g", "h"]),
    ])
    assert path_list.all_nodes == {"a", "b", "c", "d", "e", "f", "g", "h"}
    assert path_list.all_edges == [
        ("a", "e"), ("b", "e"), ("c", "f"), ("d", "f"), ("e", "g"), ("f", "g"), ("g", "h")
    ]

    topo_sort = path_list.topological_sort()
    assert [x.n_steps for x in topo_sort] == sorted([x.n_steps for x in topo_sort])

def test_PathwayList_to_graphviz():
    path_list = PL([
        P(["a", "b", "c"]),
        P(["a", "c"]),
    ])
    path_list.to_graphviz()
