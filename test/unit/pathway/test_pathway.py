from nwgraph.pathway import Pathway as P, ConvergingPathways as CP

from list_equal_omg import list_equal_omg

def test_Pathway_1():
    path = P(["a", "b"])
    assert len(path.partial_pathways) == 2
    assert path.partial_pathways[-1] == path

def test_Pathway_2():
    path = P([0, 1])
    assert len(path.partial_pathways) == 2
    assert path.partial_pathways[-1] == path

def test_Pathway_line():
    """Line pathway: a -> b -> c -> d"""
    path = P(["a", "b", "c", "d"])
    assert path.partial_pathways == [["a"], ["a", "b"], ["a", "b", "c"], ["a", "b", "c", "d"]]
    assert len(path.partial_pathways) == 4
    assert path.is_line_pathway()
    assert path.all_nodes == ["a", "b", "c", "d"]
    assert path.all_edges == [("a", "b"), ("b", "c"), ("c", "d")]

def test_Pathway_with_ConvergingPathway_start():
    """
    Converging pathways
    a --> c --\
               --> d
    b --> c --/
    """
    path = P([CP([P(["a", "c"]), P(["b", "c"])]), "d"])
    assert not path.is_line_pathway()
    assert len(path.partial_pathways) == 6
    assert path.all_nodes == ["a", "b", "c", "d"]
    assert path.all_edges == [("a", "c"), ("b", "c"), ("c", "d")]
    assert list_equal_omg(path.partial_pathways, [
        ["a"],
        ["b"],
        ["a", "c"],
        ["b", "c"],
        P([CP([P(["a", "c"]), P(["b", "c"])])]),
        P([CP([P(["a", "c"]), P(["b", "c"])]), "d"]),
    ])

def test_Pathway_one_node():
    path = P(["a"])
    assert path.is_line_pathway()
    assert len(path.partial_pathways) == 1
    assert path.all_nodes == ["a"]
    assert path.all_edges == []
    assert list_equal_omg(path.partial_pathways, P(["a"]))

def test_Pathway_ConvegingPathways_on_one_node():
    path = P([CP([P(["a", "d"]), P(["b", "d"]), P(["c", "d"])])])
    assert not path.is_line_pathway()
    assert len(path.partial_pathways) == 7
    assert path.all_nodes == ["a", "b", "c", "d"]
    assert path.all_edges == [("a", "d"), ("b", "d"), ("c", "d")]
    assert list_equal_omg(path.partial_pathways, [
        ["a"],
        ["b"],
        ["c"],
        ["a", "d"],
        ["b", "d"],
        ["c", "d"],
        P([CP([P(["a", "d"]), P(["b", "d"]), P(["c", "d"])])]),
    ])

def test_Pathway_has_output_edge():
    assert P([0, 1, 2]).has_output_edge()
    assert P(["0", "1", "2"]).has_output_edge()
    assert P([CP([P(["a", "x"]), P(["b", "x"])]), "y"]).has_output_edge()
    assert P([CP([P(["a", "x"]), P(["b", "x"])]), "y", "z"]).has_output_edge()
    assert not P(["a"]).has_output_edge()
    assert not P([CP([P(["a", "x"]), P(["b", "x"]), P(["c", "x"])])]).has_output_edge()

def test_Pathway_n_steps():
    assert P([0, 1, 2]).n_steps == 2
    assert P(["0", "1", "2"]).n_steps == 2
    assert P([CP([P(["a", "x"]), P(["b", "x"])]), "y"]).n_steps == 2
    assert P([CP([P(["a", "x"]), P(["b", "x"])]), "y", "z"]).n_steps == 3
    assert P(["a"]).n_steps == 0
    assert P([CP([P(["a", "x"]), P(["b", "x"]), P(["c", "x"])])]).n_steps == 1
    assert P([CP([P(["a", "c", "x"]), P(["b", "y", "x"]), P(["c", "a", "x"])]), "y", "z"]).n_steps == 4

def test_Pathway_n_steps_delay():
    assert P([0, 1, 2], delay=1).n_steps == 3
    assert P(["0", "1", "2"], delay=1).n_steps == 3
    assert P([CP([P(["a", "x"], delay=1), P(["b", "x"], delay=1)]), "y"], delay=1).n_steps == 4
    assert P([CP([P(["a", "x"]), P(["b", "x"])]), "y", "z"], delay=1).n_steps == 4
    assert P(["a"], delay=3).n_steps == 3
    assert P([CP([P(["a", "x"]), P(["b", "x"]), P(["c", "x"])])]).n_steps == 1
    assert P([CP([P(["a", "x"], delay=1), P(["b", "y", "x"]), P(["c", "x"], delay=1)])]).n_steps == 2
    assert P([CP([P(["a", "x"], delay=1), P(["b", "y", "x"]), P(["c", "x"], delay=1)]), "y", "z"], delay=2).n_steps == 6

def test_Pathway_to_graphviz():
    path = P(["a", "b", "c", "d"])
    path.to_graphviz()
