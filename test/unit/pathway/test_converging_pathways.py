from nwgraph.pathway import Pathway as P, ConvergingPathways as CP

from list_equal_omg import list_equal_omg

def test_ConvergingPathways_1():
    cp = CP([P(["a", "z"]), P(["b", "z"]), P(["c", "z"])])
    assert len(cp) == 3
    assert cp == [P(["a", "z"]), P(["b", "z"]), P(["c", "z"])]
    assert len(cp.partial_pathways) == 6
    assert cp.converging_node == "z"

def test_ConvergingPathways_2():
    """paths must converge to the same node"""
    try:
        _ = CP([P(["a", "z"]), P(["b", "z"]), P(["c", "w"])])
        raise Exception
    except ValueError:
        pass

def test_ConvergingPathways_3():
    path_list = CP([P(["a", "b", "z"]), P(["b", "z"], delay=1), P(["c", "z"], delay=1)])
    assert len(path_list) == 3
    assert list_equal_omg(path_list, [["a", "b", "z"], ["b", "z"], ["c", "z"]])
    assert len(path_list.partial_pathways) == 7

def test_ConvergingPathways_4():
    path = P([CP([P(["a", "p"]), P(["b", "p"]), P(["c", "p"])]), "z"])
    assert path.all_nodes == ["a", "b", "c", "p", "z"]
    assert len(path.partial_pathways) == 8
    assert list_equal_omg(path.partial_pathways, [
        ["a"],
        ["b"],
        ["c"],
        ["a", "p"],
        ["b", "p"],
        ["c", "p"],
        [CP([P(["a", "p"]), P(["b", "p"]), P(["c", "p"])])],
        P([CP([P(["a", "p"]), P(["b", "p"]), P(["c", "p"])]), "z"]),
    ])

def test_ConvergingPathways_5():
    """example 1 but with an extra converging pathway for each input node"""
    converging_path = CP([
        P([CP([P(["_a", "a"]), P(["_b", "a"]), P(["_c", "a"])]), "z"]),
        P([CP([P(["_a", "b"]), P(["_b", "b"]), P(["_c", "b"])]), "z"]),
        P([CP([P(["_a", "c"]), P(["_b", "c"]), P(["_c", "c"])]), "z"]),
    ])
    assert list_equal_omg(converging_path.partial_pathways, [
        ["_a"], ["_b"], ["_c"],
        ["_a", "a"], ["_b", "a"], ["_c", "a"],
        ["_a", "b"], ["_b", "b"], ["_c", "b"],
        ["_a", "c"], ["_b", "c"], ["_c", "c"],
        [CP([P(["_a", "a"]), P(["_b", "a"]), P(["_c", "a"])])],
        [CP([P(["_a", "b"]), P(["_b", "b"]), P(["_c", "b"])])],
        [CP([P(["_a", "c"]), P(["_b", "c"]), P(["_c", "c"])])],
        P([CP([P(["_a", "a"]), P(["_b", "a"]), P(["_c", "a"])]), "z"]),
        P([CP([P(["_a", "b"]), P(["_b", "b"]), P(["_c", "b"])]), "z"]),
        P([CP([P(["_a", "c"]), P(["_b", "c"]), P(["_c", "c"])]), "z"]),
    ])

def test_ConvergingPathways_6():
    """example 1 but with an extra converging pathway for each input node"""
    converging_path = CP([
        P([CP([P(["_a", "a"]), P(["_b", "a"]), P(["_c", "a"])]), "z"]),
        P([CP([P(["_a", "b"]), P(["_b", "b"]), P(["_c", "b"])]), "z"]),
        P([CP([P(["_a", "c"]), P(["_b", "c"]), P(["_c", "c"])]), "z"]),
    ])
    path = P([converging_path, "y"])
    assert path.all_nodes == ["_a", "_b", "_c", "a", "b", "c", "y", "z"]
    assert list_equal_omg(path.partial_pathways, [
        ["_a"], ["_b"], ["_c"],
        ["_a", "a"], ["_b", "a"], ["_c", "a"],
        ["_a", "b"], ["_b", "b"], ["_c", "b"],
        ["_a", "c"], ["_b", "c"], ["_c", "c"],
        [CP([P(["_a", "a"]), P(["_b", "a"]), P(["_c", "a"])])],
        [CP([P(["_a", "b"]), P(["_b", "b"]), P(["_c", "b"])])],
        [CP([P(["_a", "c"]), P(["_b", "c"]), P(["_c", "c"])])],
        P([CP([P(["_a", "a"]), P(["_b", "a"]), P(["_c", "a"])]), "z"]),
        P([CP([P(["_a", "b"]), P(["_b", "b"]), P(["_c", "b"])]), "z"]),
        P([CP([P(["_a", "c"]), P(["_b", "c"]), P(["_c", "c"])]), "z"]),
        [converging_path],
        path
    ])

def test_ConvergingPathways_7():
    # cps must have the same length
    try:
        _ = CP([P(["a", "b", "z"]), P(["b", "z"]), P(["c", "z"])])
        raise Exception
    except ValueError:
        pass

def test_ConvergingPathways_8():
    # cps must have the same length
    cp = CP([P(["a", "b", "z"]), P(["b", "z"]), P(["c", "z"])], add_delays=True)
    assert cp.n_steps == 2

def test_ConvergingPathways_to_graphviz():
    cp = CP([
        P([CP([P(["_a", "a"]), P(["_b", "a"]), P(["_c", "a"])]), "z"]),
        P([CP([P(["_a", "b"]), P(["_b", "b"]), P(["_c", "b"])]), "z"]),
        P([CP([P(["_a", "c"]), P(["_b", "c"]), P(["_c", "c"])]), "z"]),
    ])
    cp.to_graphviz()

if __name__ == "__main__":
    test_ConvergingPathways_1()
