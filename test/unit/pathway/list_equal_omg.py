def list_equal_omg(l1, l2):
    if len(l1) != len(l2):
        return False
    taken = [False] * len(l1)
    for i in range(len(l1)):
        found = False
        for j in range(len(l2)):
            if not taken[j] and l1[i] == l2[j]:
                found = True
                taken[j] = True
                break
        if not found:
            return False
    return sum(taken) == len(l1)
