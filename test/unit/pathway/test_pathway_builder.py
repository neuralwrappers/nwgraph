from nwgraph.pathway import pathway_builder, Pathway as P, ConvergingPathways as CP, PathwayList as PL

def test_pathway_builder_1():
    path = ["a", "b", "c"]
    assert pathway_builder(path) == P(path)

def test_pathway_builder_2():
    assert pathway_builder([[["a", "c"], ["b", "c"]], "d"]) == P([CP([P(["a", "c"]), P(["b", "c"])]), "d"])

def test_pathway_builder_3():
    path = [["a", "b"], ["c", "b"]]
    assert pathway_builder(path) == CP([P(p) for p in path])

def test_pathway_builder_4():
    pathways = [[0, 4], [1, 4], [0, 5], [1, 5], [[[0, 4], [1, 4]], 5]]
    expected = PL([P([0, 4]), P([1, 4]), P([0, 5]), P([1, 5]), P([CP([P([0, 4]), P([1, 4])]), 5])])
    assert pathway_builder(pathways) == expected

def test_pathway_builder_5():
    cp = pathway_builder([
        [[["_a", "a"], ["_b", "a"], ["_c", "a"]], "z"],
        [[["_a", "b"], ["_b", "b"], ["_c", "b"]], "z"],
        [[["_a", "c"], ["_b", "c"], ["_c", "c"]], "z"],
    ])

    epxected = CP([
        P([CP([P(["_a", "a"]), P(["_b", "a"]), P(["_c", "a"])]), "z"]),
        P([CP([P(["_a", "b"]), P(["_b", "b"]), P(["_c", "b"])]), "z"]),
        P([CP([P(["_a", "c"]), P(["_b", "c"]), P(["_c", "c"])]), "z"]),
    ])

    assert cp == epxected

def test_pathway_builder_6():
    pathways = pathway_builder([
        [[[[["a", "e"], ["b", "e"]], "g"], [[["c", "f"], ["d", "f"]], "g"]], "h"],
        [[["a", "e"], ["b", "e"]], "g", "h"],
        [[["c", "f"], ["d", "f"]], "g", "y"],
    ])

    expected = PL([
        P([
            CP([
                P([CP([P(["a", "e"]), P(["b", "e"])]), "g"]),
                P([CP([P(["c", "f"]), P(["d", "f"])]), "g"])
            ]), "h"
        ]),
        P([CP([P(["a", "e"]), P(["b", "e"])]), "g", "h"]),
        P([CP([P(["c", "f"]), P(["d", "f"])]), "g", "y"]),
    ])

    assert pathways == expected

def test_pathway_builder_7():
    pathways = pathway_builder([
        [[[[["a", "e"], ["b", "e"]], "g"], [[["c", "f"], ["d", "f"]], "g"]], "h"],
        [[["a", "e"], ["b", "e"]], "g", "h"],
        [[["c", "f"], ["d", "f"]], "g", "h"],
    ])

    expected = CP([
        P([
            CP([
                P([CP([P(["a", "e"]), P(["b", "e"])]), "g"]),
                P([CP([P(["c", "f"]), P(["d", "f"])]), "g"])
            ]), "h"
        ]),
        P([CP([P(["a", "e"]), P(["b", "e"])]), "g", "h"]),
        P([CP([P(["c", "f"]), P(["d", "f"])]), "g", "h"]),
    ])

    assert pathways == expected
