from nwgraph.pathway import PathwayList as PL, Pathway as P, ConvergingPathways as CP

def test_PathwayList_simulate_mp_1():
    path_list = PL([P(["a", "b"])])
    sim = path_list.simulate_mp()
    sim_lens = [{k: len(v) for k, v in sim_t.items()} for sim_t in sim]
    assert sim_lens == [{"a": 1, "b": 0}, {"a": 0, "b": 1}]

def test_PathwayList_simulate_mp_2():
    path_list = PL([
        P(["a", "b"]),
        P(["a", "c"]),
    ])
    sim = path_list.simulate_mp()
    sim_lens = [{k: len(v) for k, v in sim_t.items()} for sim_t in sim]
    assert sim_lens == [{"a": 1, "b": 0, "c": 0}, {"a": 0, "b": 1, "c": 1}]

def test_PathwayList_simulate_mp_3():
    path_list = PL([
        P(["a", "b", "c"]),
        P(["a", "c"]),
    ])
    sim = path_list.simulate_mp()
    sim_lens = [{k: len(v) for k, v in sim_t.items()} for sim_t in sim]
    assert sim_lens == [{"a": 1, "b": 0, "c": 0}, {"a": 0, "b": 1, "c": 1}, {"a": 0, "b": 0, "c": 1}]

def test_PathwayList_simulate_mp_4():
    path_list = PL([
        P(["a", "c"]),
        P(["b", "c"]),
    ])
    sim = path_list.simulate_mp()
    sim_lens = [{k: len(v) for k, v in sim_t.items()} for sim_t in sim]
    assert sim_lens == [{"a": 1, "b": 1, "c": 0}, {"a": 0, "b": 0, "c": 2}]

def test_PathwayList_simulate_mp_5():
    path_list = PL([
        P([CP([P(["a", "c"]), P(["b", "c"])]), "d"]),
        P(["x", "y", "d"]),
    ])
    sim = path_list.simulate_mp()
    sim_lens = [{k: len(v) for k, v in sim_t.items()} for sim_t in sim]
    assert sim_lens == [
        {"a": 1, "b": 1, "c": 0, "d": 0, "x": 1, "y": 0},
        {"a": 0, "b": 0, "c": 2, "d": 0, "x": 0, "y": 1},
        {"a": 0, "b": 0, "c": 0, "d": 2, "x": 0, "y": 0}
    ]

def test_PathwayList_simulate_mp_6():
    # the merging pattern
    path_list = PL([
        P([
            CP([
                P([CP([P(["a", "e"]), P(["b", "e"])]), "g"]),
                P([CP([P(["c", "f"]), P(["d", "f"])]), "g"])
            ]), "h"
        ]),
        P([CP([P(["a", "e"]), P(["b", "e"])]), "g", "h"]),
        P([CP([P(["c", "f"]), P(["d", "f"])]), "g", "h"]),
    ])
    sim = path_list.simulate_mp()
    sim_lens = [{k: len(v) for k, v in sim_t.items()} for sim_t in sim]
    assert sim_lens == [
        {'a': 1, 'b': 1, 'c': 1, 'd': 1, 'e': 0, 'f': 0, 'g': 0, 'h': 0},
        {'a': 0, 'b': 0, 'c': 0, 'd': 0, 'e': 2, 'f': 2, 'g': 0, 'h': 0},
        {'a': 0, 'b': 0, 'c': 0, 'd': 0, 'e': 0, 'f': 0, 'g': 2, 'h': 0},
        {'a': 0, 'b': 0, 'c': 0, 'd': 0, 'e': 0, 'f': 0, 'g': 0, 'h': 3}
    ]
