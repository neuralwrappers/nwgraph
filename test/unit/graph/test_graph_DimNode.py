from nwgraph import Graph
from nwgraph.graph.node import Node, DimNode

def test_Graph_ctor_node_type_DimNode_1():
    """DimNode but no node_args"""
    edge_indexes = [("0", "4"), ("1", "4"), ("0", "5"), ("1", "5"), ("4", "5")]
    node_types = [DimNode, DimNode, DimNode, DimNode]
    try:
        _ = Graph(edge_indexes, node_types=node_types)
        raise Exception
    except TypeError:
        pass

def test_Graph_ctor_node_type_DimNode_2():
    """DimNode with proper node_args"""
    edge_indexes = [("0", "4"), ("1", "4"), ("0", "5"), ("1", "5"), ("4", "5")]
    node_args = [{"dims": (1, )}, {"dims": (2, )}, {"dims": (3, )}, {"dims": (4, )}]
    node_types = [DimNode, DimNode, DimNode, DimNode]
    g = Graph(edge_indexes, node_types=node_types, node_args=node_args)
    for i, node_arg in enumerate(g.node_args):
        assert node_arg["dims"] == (i + 1, )

def test_Graph_ctor_node_type_DimNode_3():
    """DimNode and Node intermixed. This fails because Nodes also receive arguments"""
    edge_indexes = [("0", "4"), ("1", "4"), ("0", "5"), ("1", "5"), ("4", "5")]
    node_args = [{"dims": (1, )}, {"dims": (2, )}, {"dims": (3, )}, {"dims": (4, )}]
    node_types = [DimNode, DimNode, DimNode, Node]
    try:
        _ = Graph(edge_indexes, node_types=node_types, node_args=node_args)
        raise Exception
    except TypeError:
        pass

def test_Graph_ctor_node_type_DimNode_4():
    """DimNode and Node intermixed. No node args are provided (empty dict) for simple (Node type) nodes"""
    edge_indexes = [("0", "4"), ("1", "4"), ("0", "5"), ("1", "5"), ("4", "5")]
    node_args = [{"dims": (1, )}, {"dims": (2, )}, {}, {"dims": (4, )}, {}, {"dims": (6, )}]
    node_types = [DimNode, DimNode, Node, DimNode, Node, DimNode]
    node_names = ["0", "1", "2", "3", "4", "5"]
    g = Graph(edge_indexes, node_types=node_types, node_args=node_args, node_names=node_names)
    for i, node_arg in enumerate(g.node_args):
        assert node_arg.get("dims") == ((i + 1, ) if i not in (2, 4) else None)

def test_Graph_ctor_node_type_DimNode_5():
    """DimNode but with args provided just for one node"""
    edge_indexes = [("0", "4"), ("1", "4"), ("0", "5"), ("1", "5"), ("4", "5")]
    node_names = ["0", "1", "2", "3", "4", "5"]
    node_args = {"dims": (1, )}
    node_types = [DimNode, DimNode, DimNode, DimNode, DimNode, DimNode]
    g = Graph(edge_indexes, node_types=node_types, node_args=node_args, node_names=node_names)
    for node_arg in g.node_args:
        assert node_arg["dims"] == (1, )

def test_Graph_ctor_node_type_DimNode_6():
    """DimNode but with args and types provided just for one node"""
    edge_indexes = [("0", "4"), ("1", "4"), ("0", "5"), ("1", "5"), ("4", "5")]
    node_names = ["0", "1", "2", "3", "4", "5"]
    node_args = {"dims": (1, )}
    node_types = DimNode
    g = Graph(edge_indexes, node_types=node_types, node_args=node_args, node_names=node_names)
    for node_arg in g.node_args:
        assert node_arg["dims"] == (1, )
