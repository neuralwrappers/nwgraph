from nwgraph.graph import Graph, Edge, Node, DimNode, NodesState
import torch as tr
import pytest

def test_Graph_ctor_1():
    """specifying the node names, we impose a restriction on the number of nodes"""
    edge_indexes = [("A", "E"), ("B", "E"), ("A", "F"), ("B", "F"), ("E", "F")]
    g = Graph(edge_indexes, node_names=["A", "B", "E", "F"])

    assert len(g.nodes) == 4
    assert len(g.edges) == 5
    for node in g.nodes:
        assert isinstance(node, Node)
    for node_arg in g.node_args:
        assert node_arg == {}
    for edge in g.edges:
        assert isinstance(edge, Edge)
    # TODO: edge args
    # for edge, edge_arg in g.edge_args.items():
    #     assert edge in g.edges and edge_arg == {}
    assert isinstance(g.adjacency_matrix, tr.Tensor) and g.adjacency_matrix.shape == (4, 4)
    assert g.adjacency_matrix.sum() == 5
    assert g.adjacency_matrix[0, 2] == 1 and g.adjacency_matrix[2, 0] == 0
    assert g.adjacency_matrix[0, 3] == 1 and g.adjacency_matrix[3, 0] == 0
    assert g.adjacency_matrix[1, 2] == 1 and g.adjacency_matrix[2, 1] == 0
    assert g.adjacency_matrix[1, 3] == 1 and g.adjacency_matrix[3, 1] == 0
    assert g.adjacency_matrix[2, 3] == 1 and g.adjacency_matrix[3, 2] == 0
    assert g.n_nodes_set == 0

def test_Graph_ctor_2():
    """not specifying the node names, they are deducted from edge_indexes"""
    edge_indexes = [("0", "4"), ("1", "4"), ("0", "5"), ("1", "5"), ("4", "5")]
    g = Graph(edge_indexes)

    assert len(g.nodes) == 4
    assert len(g.edges) == 5

def test_Graph_ctor_3():
    """same as test_Graph_ctor_2 but with more nodes"""
    edge_indexes = [("A", "E"), ("B", "E"), ("A", "F"), ("B", "F"), ("E", "F")]
    g = Graph(edge_indexes, node_names=["A", "B", "C", "D", "E", "F"])

    assert len(g.nodes) == 6
    assert len(g.edges) == 5

def test_Graph_ctor_4():
    """bad example where we don't specify noe names with string edge indexes"""
    edge_indexes = [("A", "E"), ("B", "E"), ("A", "F"), ("B", "F"), ("E", "F")]
    g = Graph(edge_indexes)

    assert g.node_names == ["A", "B", "E", "F"]
    assert len(g.edges) == 5

def test_Graph_ctor_5():
    """This failed in ngclib"""
    edge_indexes = [("0", "4"), ("1", "4"), ("0", "5"), ("1", "5"), ("4", "5")]
    node_names = ["0", "1", "4", "5"]
    g = Graph(edge_indexes, node_args={"dims": (3, )}, node_names=node_names, node_types=DimNode)
    assert len(g.nodes) == 4
    assert g.node_names == ["0", "1", "4", "5"]

def test_Graph_subgraph_1():
    """subgraph by edge indexes"""
    edge_indexes = [("0", "4"), ("1", "4"), ("0", "5"), ("1", "5"), ("4", "5")]
    g = Graph(edge_indexes)
    subgraph = g.subgraph([("0", "4"), ("1", "5")])

    assert len(g.nodes) == 4
    assert len(g.edges) == 5
    assert len(subgraph.nodes) == 4
    assert len(subgraph.edges) == 2

    assert subgraph.edges[0] == g.edges[0]
    assert subgraph.edges[1] == g.edges[3]

def test_Graph_subgraph_2():
    """subgraph by edge names"""
    edge_indexes = [("0", "4"), ("1", "4"), ("0", "5"), ("1", "5"), ("4", "5")]
    g = Graph(edge_indexes)
    subgraph = g.subgraph([g.edges[0].name, g.edges[3].name])

    assert len(g.nodes) == 4
    assert len(g.edges) == 5
    assert len(subgraph.nodes) == 4
    assert len(subgraph.edges) == 2

    assert subgraph.edges[0] == g.edges[0]
    assert subgraph.edges[1] == g.edges[3]

def test_Graph_subgraph_3():
    edge_indexes = [("0", "4"), ("1", "4"), ("0", "5"), ("1", "5"), ("4", "5")]
    g = Graph(edge_indexes)
    g.nodes_state = NodesState(node_names=g.node_names, state=tr.randn(4))
    assert g.n_nodes_set == 4, g.n_nodes_set
    assert len(g.nodes) == 4
    assert len(g.edges) == 5

    subg = g.subgraph([g.edges[0].name, g.edges[3].name], prune_nodes=True)
    assert subg.n_nodes == 4
    assert subg.n_edges == 2
    assert subg.n_nodes_set == 4

    assert subg.edges[0] == g.edges[0]
    assert subg.edges[1] == g.edges[3]

    subg2 = g.subgraph([g.edges[0].name], prune_nodes=True)
    assert subg2.n_nodes == 2
    assert subg2.n_edges == 1
    assert subg2.n_nodes_set == 2

def test_Graph_graphviz_1():
    edge_indexes = [["0", "1"], ["1", "2"]]
    node_names = ["0", "1", "2"]
    g = Graph(edge_indexes=edge_indexes, node_names=node_names, n_nodes=3)
    g.to_graphviz()

def test_Graph_get_neighbours_1():
    edge_indexes = [["0", "1"], ["1", "2"]]
    node_names = ["0", "1", "2"]
    g = Graph(edge_indexes=edge_indexes, node_names=node_names, n_nodes=3)

    assert g.get_neighbours("0") == ([], ["1"])
    assert g.get_neighbours("1") == (["0"], ["2"])
    assert g.get_neighbours("2") == (["1"], [])

def test_Graph_add_nodes_1():
    edge_indexes = [["0", "1"], ["1", "2"]]
    node_names = ["0", "1", "2"]
    g = Graph(edge_indexes=edge_indexes, node_names=node_names, n_nodes=3)

    with pytest.raises(KeyError):
        g.get_neighbours("3")

    assert len(g.nodes) == 3 and g.node_names == ["0", "1", "2"] and g.nodes_state.node_names == ["0", "1", "2"]
    g.add_nodes(["3", "4"], node_types=[Node, Node], node_args=[{}, {}])
    assert len(g.nodes) == 5 and g.node_names == ["0", "1", "2", "3", "4"]

    assert g.get_neighbours("3") == ([], [])

def test_Graph_add_edges_1():
    node_names = ["0", "1", "2"]
    g = Graph(edge_indexes=[["0", "1"]], node_names=node_names, n_nodes=3)

    assert g.get_neighbours("0") == ([], ["1"])
    assert g.get_neighbours("1") == (["0"], [])
    assert g.get_neighbours("2") == ([], [])

    g.add_edges([("0", "2")])
    assert g.get_neighbours("0") == ([], ["1", "2"])
    assert g.get_neighbours("1") == (["0"], [])
    assert g.get_neighbours("2") == (["0"], [])

def test_Graph_add_edges_2():
    """adding edges w/o existing nodes should throw errors"""
    node_names = ["0", "1", "2"]
    g = Graph(edge_indexes=[["0", "1"]], node_names=node_names, n_nodes=3)
    assert g.get_neighbours("0") == ([], ["1"])
    with pytest.raises(AssertionError):
        g.add_edges([("0", "3")])

    g.add_nodes(["3"], node_types=[Node], node_args=[{}])
    g.add_edges([("0", "3")])
    assert g.get_neighbours("0") == ([], ["1", "3"])

    with pytest.raises(AssertionError):
        g.add_edges([("0", "3")])

def test_Graph_remove_edges_1():
    node_names = ["0", "1", "2"]
    g = Graph(edge_indexes=[["0", "1"], ["1", "2"]], node_names=node_names, n_nodes=3)

    assert g.get_neighbours("0") == ([], ["1"])
    assert g.get_neighbours("1") == (["0"], ["2"])
    assert g.get_neighbours("2") == (["1"], [])

    g.remove_edges([("0", "1")])
    assert g.get_neighbours("0") == ([], [])
    assert g.get_neighbours("1") == ([], ["2"])
    assert g.get_neighbours("2") == (["1"], [])

def test_Graph_remove_nodes_1():
    node_names = ["0", "1", "2"]
    g = Graph(edge_indexes=[["0", "1"], ["1", "2"]], node_names=node_names, n_nodes=3)

    assert g.get_neighbours("0") == ([], ["1"])
    assert g.get_neighbours("1") == (["0"], ["2"])
    assert g.get_neighbours("2") == (["1"], [])
    assert g.node_names == ["0", "1", "2"]

    g.remove_nodes(["0"])
    with pytest.raises(KeyError):
        assert g.get_neighbours("0") == ([], [])
    assert g.get_neighbours("1") == ([], ["2"])
    assert g.get_neighbours("2") == (["1"], [])

def test_Graph_remove_nodes_2():
    node_names = ["0", "1", "2"]
    g = Graph(edge_indexes=[["0", "1"], ["1", "2"]], node_names=node_names, n_nodes=3)
    g.add_nodes(["3", "4"], node_types=[Node, Node], node_args=[{}, {}])
    g.add_edges([("0", "3"), ("3", "4")])

    assert g.get_neighbours("0") == ([], ["1", "3"])
    assert g.node_names == ["0", "1", "2", "3", "4"]
    assert g.edge_indexes == [("0", "1"), ("1", "2"), ("0", "3"), ("3", "4")]

    g.remove_nodes(["3"])

    assert g.get_neighbours("0") == ([], ["1"])
    assert g.node_names == ["0", "1", "2", "4"]
    assert g.edge_indexes == [("0", "1"), ("1", "2")]

def test_Graph_clone_1():
    g = Graph(edge_indexes=[["0", "1"], ["1", "2"]], node_names=["0", "1", "2"], n_nodes=3)
    g2 = g.clone()
    assert g == g2

def test_Graph_clone_2():
    g = Graph(edge_indexes=[["0", "1"], ["1", "2"]], node_names=["0", "1", "2"], n_nodes=3)
    g2 = g.clone()
    g2.add_nodes(["3"], node_types=[Node], node_args=[{}])
    assert g != g2

def test_Graph_clone_3():
    g = Graph(edge_indexes=[["0", "1"], ["1", "2"]], node_names=["0", "1", "2"], n_nodes=3)
    g.add_nodes(["3"], node_types=[Node], node_args=[{}])
    g2 = g.clone()
    assert g == g2

    g.remove_nodes(["3"])
    assert g != g2

    g2.remove_nodes(["3"])
    assert g == g2

if __name__ == "__main__":
    test_Graph_remove_nodes_2()
