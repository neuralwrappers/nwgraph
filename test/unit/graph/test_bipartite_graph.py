from nwgraph.graph import BipartiteGraph
from nwgraph.graph.node import DimNode

def test_BipartiteGraph_ctor_1():
    """specifying the node names, we impose a restriction on the number of nodes"""
    edge_indexes = [("A", "E"), ("B", "E"), ("A", "F"), ("B", "F"), ("E", "F")]
    g = BipartiteGraph(edge_indexes, nodes_type_a=["A", "B"], node_names=["A", "B", "E", "F"])

    assert len(g.nodes) == 4
    assert len(g.nodes_type_a) == 2
    assert len(g.nodes_type_b) == 2
    assert sorted(g.nodes_type_a) == ["A", "B"]
    assert sorted(g.nodes_type_b) == ["E", "F"]

def test_BipartiteGraph_ctor_2():
    node_names = ["a", "b", "c", "d"]
    node_args = [{"dims": (1, )}, {"dims": (2, )}, {"dims": (3, )}, {"dims": (4, )}]
    edge_indexes = [("a", "b"), ("a", "d")]
    g = BipartiteGraph(edge_indexes=edge_indexes, node_names=node_names, node_args=node_args, node_types=DimNode,
                       nodes_type_a=["a"])
    assert g.nodes_type_a == ["a"]
    assert sorted(g.nodes_type_b) == ["b", "c", "d"]
    assert len(g.nodes) == 4

def test_BipartiteGraph_subgraph_1():
    edge_indexes = [("A", "E"), ("B", "E"), ("A", "F"), ("B", "F"), ("E", "F")]
    g = BipartiteGraph(edge_indexes, nodes_type_a=["A", "B"], node_names=["A", "B", "E", "F"])
    g2 = g.subgraph([("A", "E"), ("E", "F")], prune_nodes=False)
    assert len(g2.nodes) == 4 and g2.nodes_type_a == ["A", "B"] and sorted(g2.nodes_type_b) == ["E", "F"]

def test_BipartiteGraph_subgraph_2():
    edge_indexes = [("A", "E"), ("B", "E"), ("A", "F"), ("B", "F"), ("E", "F")]
    g = BipartiteGraph(edge_indexes, nodes_type_a=["A", "B"], node_names=["A", "B", "E", "F"])
    g2 = g.subgraph([("A", "E"), ("E", "F")], prune_nodes=True)
    assert len(g2.nodes) == 3 and g2.nodes_type_a == ["A"] and sorted(g2.nodes_type_b) == ["E", "F"]

def test_BipartiteGraph_add_nodes_1():
    edge_indexes = [["0", "1"], ["1", "2"]]
    node_names = ["0", "1", "2"]
    g = BipartiteGraph(edge_indexes=edge_indexes, node_names=node_names, n_nodes=3, nodes_type_a=["0", "1"])

    try:
        g.get_neighbours("3")
        raise ValueError
    except KeyError:
        pass

    assert len(g.nodes) == 3 and g.node_names == ["0", "1", "2"]
    g.add_nodes(["3", "4"], node_types=[DimNode, DimNode], node_args=[{"dims": (3, )}, {"dims": (4, )}])
    assert len(g.nodes) == 5 and g.node_names == ["0", "1", "2", "3", "4"]

    assert g.get_neighbours("3") == ([], [])
    assert g.nodes_type_b == ["2", "3", "4"]

def test_BipartiteGraph_add_nodes_2():
    edge_indexes = [["0", "1"], ["1", "2"]]
    node_names = ["0", "1", "2"]
    g = BipartiteGraph(edge_indexes=edge_indexes, node_names=node_names, n_nodes=3, nodes_type_a=["0", "1"])

    try:
        g.get_neighbours("3")
        raise ValueError
    except KeyError:
        pass

    assert len(g.nodes) == 3 and g.node_names == ["0", "1", "2"]
    g.add_nodes(["3", "4"], node_types=[DimNode, DimNode], node_args=[{"dims": (3, )}, {"dims": (4, )}])
    assert len(g.nodes) == 5 and g.node_names == ["0", "1", "2", "3", "4"]

    assert g.get_neighbours("3") == ([], [])
    assert g.nodes_type_b == ["2", "3", "4"]

def test_BipartiteGraph_remove_nodes_1():
    node_names = ["0", "1", "2"]
    g = BipartiteGraph(edge_indexes=[["0", "1"], ["1", "2"]], node_names=node_names, n_nodes=3, nodes_type_a=["0"])

    assert g.get_neighbours("0") == ([], ["1"])
    assert g.get_neighbours("1") == (["0"], ["2"])
    assert g.get_neighbours("2") == (["1"], [])
    assert g.node_names == ["0", "1", "2"]
    assert g.nodes_type_a == ["0"]

    g.remove_nodes(["0"])
    try:
        assert g.get_neighbours("0") == ([], [])
        raise Exception
    except KeyError:
        pass
    assert g.get_neighbours("1") == ([], ["2"])
    assert g.get_neighbours("2") == (["1"], [])
    assert g.nodes_type_a == []
